<?php
error_reporting(0);
/**
 * @package ndate
 * @version 1.0
 */
/*
Plugin Name: ndate - Nepali Date
Plugin URI: http://tuxkiddos.com/
Description: ndate is a WordPress Plugin to show nepali dates.
Author: Paras Nath Chaudhary
Version: 1.0
Author URI: http://tuxkiddos.com/
*/

/**
* var wp_version
* stores the current wordpress version
*/
require_once 'nepali_calendar.php';
global $wp_version;

/** 
* Check if the wordpress version is atleat 3.0 
*/
if ( !version_compare($wp_version,"3.0",">=") ) {
	die("Upgrade your wordpress to 3.0 or higher to use this plugin");
}

/**
* function to be executed while the plugin is activated
*/
function ndate_plugin_activate(){

}

function ndate_plugin_deactivate(){

}

function filter_ndate_convert( $date, $post_id ) {
 
	$str=explode(' ', $date);
	$cal = new Nepali_Calendar();
	$newdate=$cal->eng_to_nep($str[2],$cal->get_english_month_num(strtolower($str[0])),$str[1]);	
	return Nepali_Calendar::nepalinumber($newdate['year']).' '.$newdate['nmonth'].' '.Nepali_Calendar::nepalinumber($newdate['date']);
 
}
function filter_ndate_converter( $d, $COMMENT_ID ) {
 
	$str=explode(' ', $d);
	$cal = new Nepali_Calendar();
	$newdate=$cal->eng_to_nep($str[2],$cal->get_english_month_num(strtolower($str[0])),$str[1]);	
	return Nepali_Calendar::nepalinumber($newdate['year']).' '.$newdate['nmonth'].' '.Nepali_Calendar::nepalinumber($newdate['date']);
 
}
add_filter( 'the_date', 'filter_ndate_convert');
add_filter('get_the_date','filter_ndate_convert');

add_filter('get_comment_date','filter_ndate_converter');


register_activation_hook(__FILE__,"ndate_plugin_activate");
register_deactivation_hook(__FILE__,"ndate_plugin_deactivate");



?>